import ctypes
import os

from Class.Hexdump import Hexdump
from Class.Registry import Registry
from Class.String import String


class RegistryTools:


	@classmethod
	def recentlyDocuments(cls):

		path = r"Software\Microsoft\Windows\CurrentVersion\Explorer\RecentDocs"
		key = Registry.ROOT["HKCU"] # HKEY_CURRENT_USER (HKCU)

		outGetEnumValue = Registry.getEnumValue(path, key)

		if outGetEnumValue:
			for name, data, _ in outGetEnumValue:
				print("\t"*4 + ".::" + name + "::.\n")
				Hexdump.byte2dump(data)
				print("\n" + '-'*92 + "\n")


		
	@classmethod
	def autorunProgramsUser(cls):

		"""Display the paths of programs that are executed when the user login."""

		path = r"Software\Microsoft\Windows\CurrentVersion\Run"
		key = Registry.ROOT["HKCU"] # HKEY_CURRENT_USER (HKCU)

		outGetEnumValue = Registry.getEnumValue(path, key)

		if outGetEnumValue:
			return [(name, data, types) for name, data, types in outGetEnumValue]



	@classmethod
	def networkSsid(cls):

		"""Display the names of the WiFi networks to which the device has connected."""

		path = r"Software\Microsoft\Windows NT\CurrentVersion\NetworkList\Profiles"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		if outGetEnumKey:

			dataNetworkSSID = []

			for key, value in outGetEnumKey:

				outGetValue = Registry.getValue(value, key, "ProfileName")

				if outGetValue:
					dataNetworkSSID.append(outGetValue["ProfileName"])

			if dataNetworkSSID:
				return dataNetworkSSID
		


	@classmethod
	def usbStorageDevices(cls):

		"""Display USB Storages Devices."""

		path = r"System\ControlSet001\Enum\USBSTOR"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)
		recurse = True

		outGetEnumKey = Registry.getEnumKey(path, key, recurse)

		if outGetEnumKey:

			dataUSBStorageDevices = []

			for key, value in outGetEnumKey:

				outGetValue = Registry.getValue(value, key, "FriendlyName")

				if outGetValue:
					dataUSBStorageDevices.append(outGetValue["FriendlyName"])

			if dataUSBStorageDevices:
				return dataUSBStorageDevices



	@classmethod
	def mountedDevices(cls):

		"""Display the mounted devices."""

		print("\n.:: Display the mounted devices ::.\n")

		path = r"System\MountedDevices"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		out = Registry.getEnumValue(path, key)

		for name, data, _ in out:
			print("\t.::" + name + "::.\n")
			Hexdump.byte2dump(data)
			print("\n\t" + '-'*64 + "\n")



	@classmethod
	def autorunSoftwareLocationStartSystem(cls):

		"""Display the software locations that starts at the beginning of the system, rookits and other programs can be found in the Registry."""
		
		path = r"Software\Microsoft\Windows\CurrentVersion\Run"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumValue = Registry.getEnumValue(path, key)

		if outGetEnumValue:
			return [(name, data, types) for name, data, types in outGetEnumValue]



	@classmethod
	def software(cls):

		"""Recovers some of the software installed on the system."""

		path = r"Software"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		if outGetEnumKey:
			return [data.split("\\")[-1] for _, data in outGetEnumKey]



	@classmethod
	def sidUsers(cls):

		path = r"Software\Microsoft\Windows NT\CurrentVersion\Winlogon\UserARSO"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		if outGetEnumKey:
			return [data.split("\\")[-1] for _, data in outGetEnumKey]



	@classmethod
	def sidUsersSoftware(cls):

		"""Retrieves some of the names of the software installed on each user's system if you run it as an administrator."""

		key = Registry.ROOT["HKU"] # HKEY_USERS (HKU)
		sids = RegistryTools.sidUsers()

		dataSidUsersSoftware = {}

		for sid in sids:

			path = sid + "\\Software"

			outGetEnumKey = Registry.getEnumKey(path, key)

			if outGetEnumKey:

				dataSidUsersSoftware[sid] = []

				for _, data in outGetEnumKey:

					data = data.split("\\")[-1]

					if not data.lower().startswith("classe"):
						dataSidUsersSoftware[sid].append(data)

		if dataSidUsersSoftware:
			return dataSidUsersSoftware



	@classmethod
	def firewall(cls):

		path = r"SYSTEM\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)
		name = "EnableFirewall"

		outGetValue = Registry.getValue(path, key, name)

		if outGetValue:
			return {"Firewall": outGetValue["EnableFirewall"]}



	@classmethod
	def uninstall(cls):

		path = r"Software\Microsoft\Windows\CurrentVersion\Uninstall"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		for _, data in outGetEnumKey:
				
			print("\t.::" + data + "::.\n")

			outGetEnumValue = Registry.getEnumValue(data, key)

			if outGetEnumValue:

				for name2, data2, _ in outGetEnumValue:
					print("\t", "[*]", name2, ":", data2)

			print("\n" + '-'*90 + "\n")



	@classmethod
	def service(cls):

		path = r"System\CurrentControlSet\Services"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		if outGetEnumKey:
			return [data.split("\\")[-1] for _, data in outGetEnumKey]
			


	@classmethod
	def networkInterfaces(cls):

		"""Retrieves network interfaces."""

		path = r"SYSTEM\ControlSet001\Services\Tcpip\Parameters\Interfaces"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key, True)

		if outGetEnumKey:

			names = (
				"DhcpDefaultGateway", 
				"DhcpDomain", 
				"DhcpIPAddress", 
				"DhcpNameServer",
				"DhcpServer",
				"DhcpSubnetMask",
				"NameServer"
			)

			interface = []

			for _, data in outGetEnumKey:

				getValue = Registry.getValue(data, key, names)

				if getValue:
					
					data = {}

					for key2, value in getValue.items():
						data[key2] = value

					interface.append(data)

			if interface:
				return interface



	@classmethod
	def firewallRules(cls):

		path = r"SYSTEM\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules"
		key = Registry.ROOT["HKLM"]

		outGetEnumValue = Registry.getEnumValue(path, key)

		firewall = []

		for _, data, _ in outGetEnumValue:

			data2 = {}

			for d in data.split("|")[1:-1]:
				d2 = d.split("=")
				data2[d2[0]] = d2[1]

			firewall.append(data2)
				
		if firewall:
			return firewall



	@classmethod
	def firewallRulesRestricted(cls):

		path = r"SYSTEM\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\RestrictedServices\AppIso\FirewallRules"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumValue = Registry.getEnumValue(path, key)

		firewall = []

		for _, data, _ in outGetEnumValue:

			data2 = {}

			for d in data.split("|")[1:-1]:
				d2 = d.split("=")
				data2[d2[0]] = d2[1]

			firewall.append(data2)
				
		if firewall:
			return firewall



	@classmethod
	def computerName(cls):

		path = r"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetValue = Registry.getValue(path, key, "ComputerName")

		if outGetValue:
			return {"ComputerName":outGetValue["ComputerName"]}



	@classmethod
	def msu(cls):

		path = r"SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\Packages"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key)

		msu = []

		for _, data in outGetEnumKey:

			outGetEnumValue = Registry.getEnumValue(data, key)

			if outGetEnumValue:
				msu.append({data.split("\\")[-1]: outGetEnumValue})
			else:
				msu.append(data.split("\\")[-1])

		if msu:
			return msu


	@classmethod
	def virtualization(cls):

		path = r"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetValue = Registry.getValue(path, key, "EnableVirtualization")

		if outGetValue:
			return {"Virtualization": outGetValue["EnableVirtualization"]}



	@classmethod
	def userCurrentEnvironement(cls):

		path = r"Volatile Environment"
		key = Registry.ROOT["HKCU"] # HKEY_CURRENT_USER (HKCU)
		names = (
			"APPDATA",
			"HOMEDRIVE",
			"HOMEPATH",
			"LOCALAPPDATA",
			"LOGONSERVER",
			"USERDOMAIN",
			"USERDOMAIN_ROAMINGPROFILE",
			"USERNAME",
			"USERPROFILE"
		)

		outGetValue = Registry.getValue(path, key, names)

		if outGetValue:
			return outGetValue



	@classmethod
	def disk(cls):

		path = r"SYSTEM\ControlSet001\Enum\SCSI"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		outGetEnumKey = Registry.getEnumKey(path, key, True)

		if outGetEnumKey:

			dataDisk = []

			for _, data in outGetEnumKey:

				outGetValue = Registry.getValue(data, key, "FriendlyName")

				if outGetValue:

					if data.split("\\")[-2].startswith("Disk&Ven_"):
						if data.split("\\")[-2].split("&")[-1] != "Prod_Virtual_Disk":
							dataDisk.append(outGetValue)

			if dataDisk:
				return dataDisk



	@classmethod
	def usbStorageState(cls):

		"""Récupére l'état des ports de type USB Storage."""

		path = r"SYSTEM\CurrentControlSet\Services\USBSTOR"
		key = Registry.ROOT["HKLM"] # HKEY_LOCAL_MACHINE (HKLM)

		# Start = 3 => USB Plug Enable (Aucune Protection)
		# Start = 4 => USB Plug Disable (Protection)

		outGetValue = Registry.getValue(path, key, "Start")

		if outGetValue:
			return outGetValue