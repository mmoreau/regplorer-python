import os.path
import argparse
import sys


class Hexdump:


	@classmethod
	def file2dump(cls, path, block=16, optimize=False):
		
		"""Represents the file in hex and ascii."""


		lock = 0


		if isinstance(path, str):
			if os.path.isfile(path):
				lock += 1


		if isinstance(block, int):
			if block > 0:
				lock += 1


		if isinstance(optimize, bool):
			if optimize:
				pattern = "." * block

			lock += 1


		if lock == 3:

			_sizeFile = os.path.getsize(path)
			_paddingOffset = (_sizeFile // (_sizeFile // 16)).bit_length()
			
			with open(path, "rb") as f:

				_byte = f.read(block)
				_offset = 0

				while _byte != b"":

					outByte2Ascii = "".join([chr(data) if data in range(32, 127) else "." for data in _byte])

					if outByte2Ascii:

						outStr2space = (" " * 2).join([_byte.hex()[i:i + 2] for i in range(0, len(_byte.hex()), 2)])

						if optimize:
							if outByte2Ascii not in pattern:
								print("0x" + hex(_offset)[2:].zfill(_paddingOffset), " "*5 + outStr2space + " "*5, outByte2Ascii)
						else:
							print("0x" + hex(_offset)[2:].zfill(_paddingOffset), " "*5 + outStr2space + " "*5, outByte2Ascii)

					_offset += block
					_byte = f.read(block)



	@classmethod
	def byte2dump(cls, data, block=16):

		"""Represents the character string from byte to hex and ascii."""

		lock = 0

		if isinstance(data, bytes):
			lock += 1

		
		if isinstance(block, int):
			if block > 0:
				lock += 1

		
		if lock == 2:

			offsetHex = 0
			sizeData = len(data)
			paddingOffset = (sizeData // 16).bit_length() if sizeData > 255 else 2

			for index in range(0, sizeData, block):

				sdata = data[index: (index + block)]

				offset = "0x" + hex(offsetHex)[2:].zfill(paddingOffset)
				byte2hex = (" ".join([hex(a)[2:].zfill(2) for a in sdata])).ljust((block * 2) + (block - 1))
				byte2ascii = "".join([chr(rdata) if rdata in range(32, 127) else "." for rdata in sdata])

				print(offset, " ", byte2hex, " ", byte2ascii) # yield (offset, byte2hex, byte2ascii)

				offsetHex += 16


try:
	if len(sys.argv) in (2, 3, 4, 5, 6):

		parser = argparse.ArgumentParser()

		parser.add_argument("-p", "--path", type=str)
		parser.add_argument("-s", "--byte", type=str)
		parser.add_argument("-b", "--block", type=int)
		parser.add_argument("-o", "--optimize", action="store_true")
		parser.add_argument("-v", "--version", action="store_true")

		args = parser.parse_args()

		_path = args.path
		_byte = args.byte
		_block = args.block
		_optimize = args.optimize
		_version = args.version


		if _path:
			if _block:
				Hexdump.file2dump(_path, _block, _optimize)
			else:
				Hexdump.file2dump(_path, 16, _optimize)


		if _byte:
			if _block:
				Hexdump.byte2dump(_byte.encode(), _block)
			else:
				Hexdump.byte2dump(_byte.encode(), 16)


		if _version:
			
			print("""
			                    _                       
			  /\  /\_____  ____| |_   _ _ __ ___  _ __  
			 / /_/ / _ \ \/ / _` | | | | '_ ` _ \| '_ \ 
			/ __  /  __/>  < (_| | |_| | | | | | | |_) |
			\/ /_/ \___/_/\_\__,_|\__,_|_| |_| |_| .__/ 
			                                     |_|\n
			Version : 0.2.0 - Developed By Maxime Moreau
            """)
except:
	pass